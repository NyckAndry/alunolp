Listagem de Pokémons feita por meio de aplicação Delphi.
Contém:
~ Um TList capaz de armazenar o nome de personagens;
~ Um TEdit para entrada do nome e um TButton para inserir na lista;
~ Um TButton responsável por excluir o item selecionado no TList;
~ Um botão capaz de excluir todos os personagens da lista;