unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.StdCtrls,
  Vcl.Imaging.jpeg, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    ListBox1: TListBox;
    TButton: TButton;
    TButton3: TButton;
    TButton2: TButton;
    TEdit: TEdit;
    Image2: TImage;
    Button1: TButton;
    Button2: TButton;
    procedure TButtonClick(Sender: TObject);
    procedure TButton2Click(Sender: TObject);
    procedure TButton3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
   ListBox1.Items[ListBox1.ItemIndex] := TEdit.Text;
   TEdit.Clear;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
 Listbox1.Items.SaveToFile ('D:\04-Informática-3\3-51\Davi_Nyck_Thai\alunolp\Listas\Dados_Lista.txt');
end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex > -1 then
  begin
   Button1.Enabled:=True;
  end;
end;

procedure TForm1.TButton2Click(Sender: TObject);
begin
 ListBox1.Items.Delete(ListBox1.ItemIndex);
end;

procedure TForm1.TButton3Click(Sender: TObject);
begin
 ListBox1.Items.Clear;
end;

procedure TForm1.TButtonClick(Sender: TObject);
begin
 if Trim(TEdit.text) = '' then
      begin
        ShowMessage('INVÁLIDO!');
      end
 else begin
        ListBox1.Items.Add (TEdit.Text);
        TEdit.Clear;
      end;
 end;


end.
